#!/bin/bash
forward=$(cat input.txt | grep forward | awk '{ s+= $2 } END { print s }')
down=$(cat input.txt | grep down | awk '{ s+= $2 } END { print s }')
up=$(cat input.txt | grep up | awk '{ s+= $2 } END { print s }')

depth=$(($down-$up))
part01=$(($depth * $forward))

echo part01 : ${part01}


forward=0
depth=0
aim=0

while read key value; do

    case $key in 
        forward)
            forward=$(($forward+$value)) 
            depth=$(($depth+$aim*$value))
            ;;
        up)
            aim=$(($aim-$value))
            ;;
        down)
            aim=$(($aim+$value))
            ;;
        *)
            echo unexpected $key $value
            ;;
    esac
done < input.txt

echo part02 : $(($depth*$forward))
